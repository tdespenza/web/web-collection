/**
 * Created by tdespenza on 8/20/16.
 */

app.directive("GroceryItem", function() {
  return {
    restrict: 'E',
    templateUrl: 'views/groceryItem.html'
  };
});