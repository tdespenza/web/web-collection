/**
 * Created by tdespenza on 8/17/16.
 */

app.service("GroceryService", function ($http) {
  var groceryService = {};

  groceryService.groceryItems = [];

  $http.get("data/server_data.json")
    .success(function(data) {
      groceryService.groceryItems = data;

      for (var item in groceryService.groceryItems) {
        groceryService.groceryItems[item].date = new Date(groceryService.groceryItems[item].date);
      }
    })
    .error(function(data, status) {
      console.log("something went wrong");
    });

  groceryService.getNewId = function () {
    if (groceryService.newId) {
      return groceryService.newId++;
    }

    var maxId = _.max(groceryService.groceryItems, function (entry) {
      return entry.id;
    });

    groceryService.newId = maxId.id + 1;
    return groceryService.newId;
  };

  groceryService.findById = function (id) {
    for (var item in groceryService.groceryItems) {
      if (groceryService.groceryItems[item].id === id) {
        return groceryService.groceryItems[item];
      }
    }
  };

  groceryService.remove = function (entry) {
    var index = groceryService.groceryItems.indexOf(entry);

    groceryService.groceryItems.splice(index, 1);
  };

  groceryService.markCompleted = function (entry) {
    entry.completed = !entry.completed;
  };

  groceryService.save = function (entry) {
    var updatedItem = groceryService.findById(entry.id);

    if (updatedItem) {
      updatedItem.completed = entry.completed;
      updatedItem.name = entry.name;
      updatedItem.date = entry.date;

    } else {
      entry.id = groceryService.getNewId();
      groceryService.groceryItems.push(entry);
    }
  };

  return groceryService;
});