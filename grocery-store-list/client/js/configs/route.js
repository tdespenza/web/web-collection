/**
 * Created by tdespenza on 8/16/16.
 */

app.config(function($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/groceryList.html',
      controller: 'HomeController'
    })
    .when('/addItem', {
      templateUrl: 'views/addItem.html',
      controller: 'GroceryListController'
    })
    .when('/addItem/edit/:id', {
      templateUrl: 'views/addItem.html',
      controller: 'GroceryListController'
    })
    .otherwise({
      redirectTo: '/'
    });
});