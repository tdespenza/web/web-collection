/**
 * Created by tdespenza on 8/16/16.
 */

app.controller("HomeController", ["$scope", "GroceryService", function ($scope, GroceryService) {
  $scope.groceryItems = GroceryService.groceryItems;

  $scope.remove = function (entry) {
    GroceryService.remove(entry);
  };

  $scope.markCompleted = function (entry) {
    GroceryService.markCompleted(entry);
  };

  $scope.$watch(function () {
      return GroceryService.groceryItems;
    },
    function (groceryItems) {
      $scope.groceryItems = groceryItems;
    });
}]);