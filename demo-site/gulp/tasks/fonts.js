'use strict';

const config = require('../config'),
  _ = require('underscore');

if (_.isNull(config) || _.isUndefined(config)) {
  return;
}

const gulp = require('gulp'),
  path = require('path'),
  browserSync = require('browser-sync').create(),
  paths = {
    src: path.join(config.root.src, config.tasks.fonts.src, '/**'),
    dest: path.join(config.root.dest, config.tasks.fonts.dest)
  },

  fontsTask = function() {
    return gulp.src(paths.src)
      .pipe(gulp.dest(paths.dest))
      .pipe(browserSync.stream());
  };

gulp.task('fonts', fontsTask);

module.exports = fontsTask;