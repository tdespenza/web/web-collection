'user strict';

const config = require('../config'),
  _ = require('underscore');

if (_.isNull(config) || _.isUndefined(config)) {
  return;
}

const gulp = require('gulp'),
  path = require('path'),
  del = require('del'),
  paths = {
    styles: path.join(config.root.dest, config.tasks.styles.dest),
    images: path.join(config.root.dest, config.tasks.images.dest),
    scripts: path.join(config.root.dest, config.tasks.scripts.dest),
    fonts: path.join(config.root.dest, config.tasks.fonts.dest)
  }

  _cleanImagesTask = function() {
    return del([path.join(paths.images, '/**'), '!' + paths.images]).then(paths => {
      console.log('Cleaned images:\n', paths.join('\n'));
    });
  },

  _cleanFontsTask = function() {
    return del([path.join(paths.fonts, '/**'), '!' + paths.fonts]).then(paths => {
      console.log('Cleaned fonts:\n', paths.join('\n'));
    });
  },

  _cleanScriptsTask = function() {
    return del([path.join(paths.scripts, '/**'), '!' + paths.scripts]).then(paths => {
      console.log('Cleaned scripts:\n', paths.join('\n'));
    });
  },

  _cleanStylesTask = function() {
    return del([path.join(paths.styles, '/**'), '!' + paths.styles]).then(paths => {
      console.log('Cleaned styles:\n', paths.join('\n'));
    });
  },

  cleanTask = function(callback) {
    _cleanImagesTask();
    _cleanFontsTask();
    _cleanScriptsTask();
    _cleanStylesTask();
    callback();
  };

gulp.task('clean', cleanTask);

module.exports = cleanTask;