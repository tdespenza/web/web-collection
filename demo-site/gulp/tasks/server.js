'use strict';

const config = require('../config'),
  serverConfig = require('../configs/server'),
  util = require('../utils/utilities');

if (util.isEmptyArray([config, serverConfig])) {
  return;
}

const gulp = require('gulp'),
  nodemon = require('gulp-nodemon'),
  path = require('path'),
  browserSync = require('browser-sync').create(),
  paths = {
    styles: path.join(config.root.src, config.tasks.styles.src, '/**'),
    images: path.join(config.root.src, config.tasks.images.src, '/**'),
    scripts: path.join(config.root.src, config.tasks.scripts.src, '/**'),
    fonts: path.join(config.root.src, config.tasks.fonts.src, '/**'),
    templates: path.join(config.tasks.templates.src, '/**')
  },

  _nodemonTask = function (callback) {
    var called = false;

    return nodemon(serverConfig.nodemon)
        .on('start', () => {
          if (!called) {
            called = true;
            console.log('started ' + serverConfig.nodemon.script);
            callback();
          }
        })
        .on('restart', () => {
          setTimeout(() => {
            browserSync.reload({stream: true});
          }, 1000);
        });
  },

  _browserSyncTask = function() {
    serverConfig.browserSync.files.push(paths.styles);
    serverConfig.browserSync.files.push(paths.images);
    serverConfig.browserSync.files.push(paths.scripts);
    serverConfig.browserSync.files.push(paths.fonts);
    serverConfig.browserSync.files.push(paths.templates);

    browserSync.init(serverConfig.browserSync);
    browserSync.watch(paths.styles).on('change', browserSync.reload);
    browserSync.watch(paths.scripts).on('change', browserSync.reload);
    browserSync.watch(paths.images).on('change', browserSync.reload);
    browserSync.watch(paths.fonts).on('change', browserSync.reload);
    browserSync.watch(paths.templates).on('change', browserSync.reload);
  },

  serverTask = function() {
    return _nodemonTask(_browserSyncTask());
  };

gulp.task('server', serverTask);

module.exports = serverTask;