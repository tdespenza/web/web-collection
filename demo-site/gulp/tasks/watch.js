'user strict';

const config = require('../config'),
  _ = require('underscore');

if (_.isNull(config) || _.isUndefined(config)) {
  return;
}

const gulp = require('gulp'),
  path = require('path'),
  browserSync = require('browser-sync').create(),
  paths = {
    styles: path.join(config.root.src, config.tasks.styles.src, '/**/*'),
    images: path.join(config.root.src, config.tasks.images.src, '/**/*'),
    scripts: path.join(config.root.src, config.tasks.scripts.src, '/**/*'),
    fonts: path.join(config.root.src, config.tasks.fonts.src, '/**/*'),
    templates: path.join(config.root.src, config.tasks.templates.src, '/**/*')
  },

  watchTask = function() {
    gulp.watch(paths.scripts, ['scripts']);
    gulp.watch(paths.styles, ['styles']);
    gulp.watch(paths.images, ['images']);
    gulp.watch(paths.fonts, ['fonts']);
  };

gulp.task('watch', watchTask);

module.exports = watchTask;