 'use strict';

const config = require('../config'),
  scriptsConfig = require('../configs/scripts'),
  _util = require('../utils/utilities');

if (_util.isEmptyArray([config, scriptsConfig])) {
  return;
}


var gulp = require('gulp'),
  jshint = require('gulp-jshint'),
  util = require('gulp-util'),
  rename = require('gulp-rename'),
  browserify = require('browserify'),
  babelify = require('babelify'),
  source = require('vinyl-source-stream'),
  assign = require('lodash.assign'),
  path = require('path'),
  multi = require('multi-bundle'),
  bromote = require('bromote'),
  browserSync = require('browser-sync').create(),
  stream = require('stream'),
  paths = {
    src: path.join(config.root.src, config.tasks.scripts.src, '/**/*.js'),
    dest: path.join(config.root.dest, config.tasks.scripts.dest)
  },

  scriptsTask = function() {
    var entry = scriptsConfig.browserify.options.entries[0],
      bundler = browserify(scriptsConfig.browserify.options),
      passThrough = new stream.PassThrough();

    bromote(bundler, scriptsConfig.bromoteOptions, function (err) {
      if (err) {
        return console.error(err);
      }

      return bundler
        .transform(babelify)
        .plugin('factor-bundle', scriptsConfig.bundles)
        .plugin('minifyify', {
          'map': entry,
          'output': path.join(paths.dest, entry.replace('.js', '.bundle.map.json'))
        })
        .plugin('browserify-bower', scriptsConfig.browserify.plugin.browserifyBower)
        .on('log', util.log)
        .on('error', util.log.bind(util, 'Browserify Error'))
        .bundle()
        .pipe(passThrough)
        .pipe(source(entry))
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(rename(scriptsConfig.rename))
        .pipe(gulp.dest(paths.dest))
        .pipe(browserSync.stream());
    });

    return passThrough;
  };

gulp.task('scripts', scriptsTask);

module.exports = scriptsTask;