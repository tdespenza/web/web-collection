'use strict';

const config = require('../config'),
  stylesConfig = require('../configs/styles'),
  util = require('../utils/utilities');

if (util.isEmptyArray([config, stylesConfig])) {
  return;
}


const gulp = require('gulp'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  concat = require('gulp-concat'),
  path = require('path'),
  browserSync = require('browser-sync').create(),
  paths = {
    src: path.join(config.root.src, config.tasks.styles.src, '/**'),
    dest: path.join(config.root.dest, config.tasks.styles.dest)
  },

  stylesTask = function() {
    return gulp.src(paths.src)
      .pipe(sourcemaps.init())
      .pipe(sass(stylesConfig.sassOptions))
      .pipe(concat(stylesConfig.outputName))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(paths.dest))
      .pipe(browserSync.stream());
  };

gulp.task('styles', stylesTask);

module.exports = stylesTask;