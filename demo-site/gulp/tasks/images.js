'use strict';

const config = require('../config'),
  imageConfig = require('../configs/images'),
  util = require('../utils/utilities');

if (util.isEmptyArray([config, imageConfig])) {
  return;
}

const gulp = require('gulp'),
  imagemin = require('gulp-imagemin'),
  responsive = require('gulp-responsive'),
  pngquant = require('imagemin-pngquant'),
  del = require('del'),
  path = require('path'),
  runSequence = require('run-sequence'),
  browserSync = require('browser-sync').create(),
  paths = {
    src: path.join(config.root.src, config.tasks.images.src),
    dest: path.join(config.root.dest, config.tasks.images.dest)
  },

  _responsiveBgTask = function() {
    return gulp.src(path.join(paths.src, 'bg/**'))
      .pipe(responsive(imageConfig.responsive.config.background, imageConfig.responsive.options))
      .pipe(gulp.dest(path.join(paths.src, 'optimized/bg')));
  },

  _responsivePhotoTask = function() {
    return gulp.src(path.join(paths.src, 'photo/**'))
      .pipe(responsive(imageConfig.responsive.config.photo, imageConfig.responsive.options))
      .pipe(gulp.dest(path.join(paths.src, 'optimized/photo')));
  },

  _copyLogoTask = function() {
    return gulp.src(path.join(paths.src, 'logo/**'))
      .pipe(gulp.dest(path.join(paths.src, 'optimized/logo')));
  },

  _copyIconsTask = function() {
    return gulp.src(path.join(paths.src, 'icon/**'))
      .pipe(gulp.dest(path.join(paths.src, 'optimized/icon')));
  },

  _copysSvgTask = function() {
    return gulp.src(path.join(paths.src, 'svg/**'))
      .pipe(gulp.dest(path.join(paths.src, 'optimized/svg')));
  },

  _deleteOptimizedFolderTask = function() {
    return del([path.join(paths.src, 'optimized')]).then(paths => {
      console.log('Cleaned optimized folder:\n', paths.join('\n'));
    });
  },

  _optimize = function() {
    imageConfig.imagemin.use.push(pngquant());

    return gulp.src(path.join(paths.src, 'optimized', '/**'))
      .pipe(imagemin(imageConfig.imagemin))
      .pipe(gulp.dest(paths.dest))
      .pipe(browserSync.stream());
  },

  imagesTask = function(callback) {
    runSequence('clean-optimize',
                ['backgrounds', 'logos', 'photos', 'icons', 'svgs'],
                'optimize',
                'clean-optimize',
                callback);
  };

gulp.task('images', imagesTask);
gulp.task('backgrounds', _responsiveBgTask);
gulp.task('photos', _responsivePhotoTask);
gulp.task('logos', _copyLogoTask);
gulp.task('icons', _copyIconsTask);
gulp.task('svgs', _copysSvgTask);
gulp.task('clean-optimize', _deleteOptimizedFolderTask);
gulp.task('optimize', _optimize);

module.exports = imagesTask;