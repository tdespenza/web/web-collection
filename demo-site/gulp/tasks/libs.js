'user strict';

const config = require('../config'),
  libsConfig = require('../configs/libs'),
  util = require('../utils/utilities');

if (util.isEmptyArray([config, libsConfig])) {
  return;
}

const gulp = require('gulp'),
  path = require('path'),
  install = require('gulp-install'),
  symlink = require('gulp-sym'),
  shell = require('gulp-shell'),
  minimist = require('minimist'),
  paths = {
    modules: {
      src: libsConfig.modules.src,
      dest: path.join(config.root.dest, libsConfig.modules.dest)
    },
    components: {
      src: libsConfig.components.src,
      dest: path.join(config.root.dest, libsConfig.components.dest)
    }
  },
  linkPaths = {
    src: [],
    dest: []
  },

  _createLinkPaths = function() {
    var dependencies = libsConfig.modules.dependencies;

    dependencies.forEach(function(dependency){
      linkPaths.src.push(path.join(paths.modules.src, dependency));
      linkPaths.dest.push(path.join(paths.modules.dest, dependency));
    });

    linkPaths.src.push(paths.components.src);
    linkPaths.dest.push(paths.components.dest);
  },

  _createUnlinkCommands = function() {
    var unlinkCommands = [];

    linkPaths.dest.forEach(function(destination) {
      unlinkCommands.push('unlink ' + destination);
    });

    return unlinkCommands;
  },

  _installLibsTask = function(callback) {
    //gulp.src(config.tasks.libs.install).pipe(install());
    return callback();
  },

  _symlinkLibsTask = function() {
    _createLinkPaths();

    return gulp.src(linkPaths.src)
      //.pipe(shell(_createUnlinkCommands()))
      .pipe(symlink(linkPaths.dest, libsConfig.simlink));
  },

  libsTask = function() {
    return _installLibsTask(_symlinkLibsTask);
  };

gulp.task('libs', libsTask);

module.exports = libsTask;