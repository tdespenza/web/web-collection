const _ = require('underscore');

function isEmpty(object) {
  return _.isEmpty(object) || _.isNull(object) || _.isUndefined(object);
}

module.exports = {
  isEmptyArray: function(array) {
    var result = false;

    if (isEmpty(array)) {
      return true;
    }

    _.each(array, function(value) {
      if (isEmpty(value)) {
        result = true;
        return false;
      }
    });

    return result;
  }
}