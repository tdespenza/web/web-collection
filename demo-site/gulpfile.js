'use strict';

var runSequence = require('run-sequence'),
  gulp = require('./gulp')([
      'clean',
      'fonts',
      'images',
      'libs',
      'server',
      'scripts',
      'styles',
      'watch'
  ]);
 
gulp.task('default', function(callback) {
  runSequence('clean',
              'libs',
              ['fonts', 'images', 'styles', 'scripts'],
              'watch',
              'server',
              callback);
});