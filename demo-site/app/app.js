'use strict';

var express = require('express'),
  fs = require('fs'),
  path = require('path'),
  favicon = require('serve-favicon'),
  logger = require('morgan'),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
  fs = require('fs-extra'),
  compression = require('compression'),
  compass = require('node-compass'),
  config = require('config'),
  googleapis = require('googleapis'),
  googleAuth = require('google-auth-library'),
  PouchDB = require('pouchdb'),
  app = express();

// application configuration
app.locals.config = config;

// setup db sync
var database = config.database;
fs.ensureDirSync(database.local);

var local = new PouchDB(database.local),
  remote = new PouchDB(database.protocol + database.host + ':' + database.port + '/' + database.db);

app.locals.pouchdb = local;

local.sync(remote, {
  live: true,
  retry: true

}).on('change', function (change) {
  console.log('syncing...');
  console.log(change);

}).on('paused', function (info) {
  console.log('sync paused.');

  if (info) {
    console.log(info);
  }

}).on('active', function (info) {
  console.log('sync resumed.');
  
  if (info) {
    console.log(info);
  }

}).on('error', function (err) {
  console.log('sync error!');
  console.log(err);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(path.join(__dirname, 'public', './images/icon/web/favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(compression());

app.use(compass({
  project: path.join(__dirname, 'assets'),
  css: path.join(__dirname, '../public/stylesheets'),
  sass: path.join(__dirname, 'styles')
}));

app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/contact', require('./routes/contact'));
app.use('/portfolio', require('./routes/portfolio'));
app.use('/work-history', require('./routes/work-history'));
app.use('/photos', require('./routes/photos'));

// Register Google APIs
/*var client;
googleapis.discover('drive', 'v2')
          .execute(function(err, data) {
            client = data;
          });

var scopes = [
  'https://www.googleapis.com/auth/drive.photos.readonly'
]

fs.readFile('../config/google-drive.json', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading client secret file: ' + err);
    return;
  }

  authorize(JSON.parse(content), listFiles);
}

function authorize(credentials, callback) {
  var auth = new googleAuth();

  auth.fromJSON(credentials, function (err) {
    done();
  });
}*/

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;

app.listen(5000);
