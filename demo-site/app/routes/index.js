'use strict';

var express = require('express'),
  router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  var db = req.app.locals.pouchdb;

  db.get('index').then(function (doc) {
    res.render('index', {title: doc.title});

  }).catch(function (err) {
    console.log('error getting index document');
    console.log(err);
  });
});

router.get('/photos', function (req, res, next) {
  var db = req.app.locals.pouchdb,
    options = {limit: 16};

  db.allDocs(options).then(function (doc) {
    if (doc && doc.rows.length > 0) {
      options.startkey = doc.rows[doc.rows.length - 1];
      options.skip = 1;
    }
    console.log('doc');
    console.log(doc);
    res.json(doc);

  }).catch(function (err) {
    console.log('error getting index photos');
    console.log(err);
    res.status(500).end();
  });
});

router.get('/db', function(req, res, next) {
  res.json(JSON.stringify(req.app.locals.config.database));
});

module.exports = router;
