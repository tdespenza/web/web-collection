'use strict';

var express = require('express'),
  router = express.Router(),
  PouchDB = require('pouchdb'),
  JSPath = require('jspath');

router.get('/', function(req, res, next) {
  var db = req.app.locals.pouchdb;

  db.get('work-history').then(function(doc) {
    res.render('work-history', doc);
  })
  .catch(function (err) {
    console.log('error getting work history document');
    console.log(err);
  });
});

router.get('/locations', function(req, res, next) {
  var db = req.app.locals.pouchdb;

  db.get('work-history').then(function(doc) {
    res.json(JSPath.apply('..location', doc).filter(function(element, index, array) {
      return arrar.indexOf(element) === index;
    }));
  })
  .catch(function (err) {
    console.log('error getting locations');
    console.log(err);
  });
});

module.exports = router;
