import PouchDB from 'pouchdb';
import _ from 'underscore';
import $ from 'jquery';

export default class DB {
  constructor() {
    if (!_.isUndefined(window)) {
      $.get('/db', function(data) {
        let database = JSON.parse(data);

        window.PouchDB = PouchDB.defaults({
          name: database.local
        });
      });
    }
  }
}
