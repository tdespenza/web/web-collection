import Hand from './hand';
import moment from 'moment';
import $ from 'jquery';

export default class Clock {
  constructor() {
    this.secondHand = new Hand();
    this.minuteHand = new Hand();
    this.hourHand = new Hand();

    this.updateTime();
  }

  updateTime() {
    var now = moment().format('hhmmssdA');
    this.rotateHands(now[4] + now[5], now[2] + now[3], now[0] + now[1]);
    setTimeout(this.updateTime.bind(this), 1000);
  }

  rotateHands(second, minute, hour) {
    if (second < 1) {
      this.secondHand.increment();

      if (minute < 1) {
        this.minuteHand.increment();

        if (hour < 1) {
          this.hourHand.increment();
        }
      }
    }

    this.secondHand.calculateAngle(60, second);
    this.minuteHand.calculateAngle(60, minute);
    this.hourHand.calculateAngle(12, hour);

    $('.seconds').css(this.secondHand.transform());
    $('.minutes').css(this.minuteHand.transform());
    $('.hours').css(this.hourHand.transform());
  }
}