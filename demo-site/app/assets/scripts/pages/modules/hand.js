export default class Hand {
  constructor() {
    this.angle = 0;
    this.rotations = 0;
  }

  calculateAngle(steps, hand) {
    this.angle = 360 / steps * hand + (360 * this.rotations);
  }

  transform() {
    return {
      '-webkit-transform': 'rotate(' + this.angle + 'deg)',
      '-khtml-transform': 'rotate(' + this.angle + 'deg)',
      '-moz-transform': 'rotate(' + this.angle + 'deg)',
      '-ms-transform': 'rotate(' + this.angle + 'deg)',
      '-o-transform': 'rotate(' + this.angle + 'deg)',
      'transform': 'rotate(' + this.angle + 'deg)'
    };
  }

  increment() {
    this.rotations++;
  }
}