import bromote from 'bromote'; 
import $ from 'jquery';
//import google from 'googleapis';

export default class Map {
  constructor(url) {
    this.initMap(url);
  }

  initMap(url) {
    let self = this;
    
    $.get(url, function(data) {
      bromote.googleMaps(function(google) {
        let locations = [],
          geocoder = new google.maps.Geocoder();

        $.each(data, function(index, value) {
          let location = value.replace(/[~\[\]:\.]/ig, '');
          location = location.replace(/\-/ig, ' ').trim();

          if (location && location.toLowerCase() !== 'global') {
            locations.push(location);
          }
        });

        self.geocode(locations, geocoder, new google.maps.Map($('.map')[0], {
          zoom: 6,
          center: {lat: 39.8282, lng: 98.5795}
        }));
      });
    });
  }

  geocode(locations, geocoder, map) {
    $.each(locations, function(index, location) {
      bromote.googleMaps(function(google) {
        geocoder.geocode({'address': location}, function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);

            let marker = new google.maps.Marker({
              map: map,
              position: results[0].geometry.location
            });

          } else {
            console.log(`Geocode was not successful for the following reason: ${status}`);
          }
        });
      });
    });
  }
}