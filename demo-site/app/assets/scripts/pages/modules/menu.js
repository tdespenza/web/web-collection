import _ from 'underscore';
import $ from 'jquery';

export default class Menu {
  constructor() {
    let path = window.location.pathname.toLowerCase();

    $('.nav > li > a').each(function() {
      var href = $(this).attr('href');

      if (!_.isNull(href) && href.toLowerCase() === path) {
        $(this).parent().addClass('active');
        return;
      }
    });
  }
}