import Common from './common';
import Clock from './modules/clock';
import Bootstrap from 'bootstrap-sass';
import flip from 'flip';
import bridget from 'jquery-bridget';
import Packery from 'packery';
import Draggabilly from 'draggabilly';
//import IScroll from 'iscroll';

export default class Index extends Common {
  constructor() {
    super();

    $.bridget('packery', Packery);

    new Clock();
    //this.initCard();
    this.initPhotoGrid();
    
    /*this.iScroll = new IScroll('.wrapper', {
      mouseWheel: true,
      scrollbars: false,
      dataset: this.requestPhotos,
      dataFiller: this.updateContent,
      cacheSize: 1000
    });*/
  }

  initCard() {
    let card = '.card';
    let cardContentFront = `${card} .front .card-content`;
    let cardContentBack = `${card} .back .card-content`;

    $(card).flip({
      trigger: 'manual'
    });

    let frontWidth = $(cardContentFront).width();
    let frontPaperCardHeight = $(cardContentFront).height();
    let frontIronImageHeight = $(`${cardContentFront} iron-image`).height();

    $(cardContentBack).width(frontWidth);
    $(cardContentBack).height(frontPaperCardHeight);
    $(cardContentBack).height(frontIronImageHeight);
    $(`${cardContentBack} p`).height(frontIronImageHeight);

    let hoverIn = () => {
      $(cardContentFront).css('opacity', '0.5');
      $(card).flip(true, () => {
        //$(`${cardContentBack} p`).show('400');
      });
    };

    let hoverOut = () => {
      //$(`${cardContentBack} p`).hide('400', () => {
        $(cardContentFront).css('opacity', '1');
        $(card).flip(false);
      //});
    };

    $(card).hover(hoverIn, hoverOut);
    //$(`${cardContentBack} p`).hide();
  }

  initPhotoGrid() {
    let $container = $('.list-group').packery({
      columnWidth: ($('.list-group').width() / 4),
      rowHeight: 0
    });

    $('.list-group-item').each(function(index, element) {
      $container.packery('bindDraggabillyEvents', new Draggabilly(element, {
        containment: '.list-group'
      }));
    });

    /*$('.photo').click(function() {
      console.log('it was clicked');
      let $parent = $(this).parent();

      if ($parent.hasClass('expanded')) {
        $parent.removeClass('expanded');

      } else {
        $parent.addClass('expanded');
      }

      $container.packery();
    });*/
  }

  requestPhotos(start, count) {
    console.log(start);
    console.log(count);
    $.get('/photos', (data) => {
      console.log('getting data');
      data = JSON.parse(data);
      this.iScroll.updateCache(start, data);
    });
  }

  updateContent(element, data) {
    console.log('data');
    console.log(data);
    $(element).append(this.photoCard(data));
  }

  photoCard(image) {
    return `<div class='list-group-item'>
              <paper-card elevation='5' animatedShadow='true' class='transparent-bg' aria-label='${image.name}'>
                <div class='card-content'>
                  <picture>
                    <source type='image/webp' srcset='/images/photo/${image.name}-480w.webp' media='(min-width: 1441px)'/>
                    <source type='image/webp' srcset='/images/photo/${image.name}-300w.webp' media='(min-width: 1281px)'/>
                    <source type='image/webp' srcset='/images/photo/${image.name}-150w.webp'/>
                    <source srcset='/images/photo/${image.name}-480w.jpg' media='(min-width: 1441px)'/>
                    <source srcset='/images/photo/${image.name}-300w.jpg' media='(min-width: 1281px)'/>
                    <img srcset='/images/photo/${image.name}-150w.jpg' alt='${image.name}'/>
                  </picture>
                </div>
              </paper-card>
            </div>`;
  }
}

$(() => {
  $(window).load(() => {
    new Index();
  });
});
