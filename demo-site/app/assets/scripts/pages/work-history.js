import Common from './common';
import Map from './modules/map';
import masonry from 'masonry-layout';
import bootstrap from 'bootstrap-sass';

export default class WorkHistory extends Common {
  constructor() {
    super();

    if (window.location.pathname === '/work-history') {
      new Map('/work-history/locations');
    }

    $('.nav-item, .sub-nav-item').click(function() {
      var href = $(this).attr('href'),
        options = {
          scrollTop: $(href).offset().top
        };

      $('html, body').animate(options, 'slow');
    });
  }
}

$(function() {
  new WorkHistory();
});