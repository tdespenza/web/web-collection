import DB from './modules/db';
import Menu from './modules/menu';
import Background from './modules/background';

export default class Common {
  constructor() {
    new DB();
    new Menu();
    new Background();
  }
}