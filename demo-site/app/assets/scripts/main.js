import Contact from './pages/contact';
import Index from './pages/index';
import Portfolio from './pages/portfolio';
import WorkHistory from './pages/work-history';
import $ from 'jquery';

export default class Main {
  constructor() {
    new Contact();
    new Index();
    new Portfolio();
    new WorkHistory();
  }
}

$(function() {
  new Main();
});