{
  "title": ": WORK-HISTORY:",
  "bio": {
       "name": ": Tyshawn-LeMarcus: Despenza.",
       "role": ": FREELANCER.",
       "contacts": {
           "mobile": "(773) 234-9358.",
           "email": "tdespenza@mobilecapptivate.com.",
           "github": "tdespenza.",
           "linkedin": "forthetld.",
           "location": "~CHICAGO,-~[IL]LINOIS."
       },
       "welcomeMessage": ": WELCOME.",
       "skills": [
           "hibernate",
           "java",
           "sass",
           "spring",
           "mybatis",
           "compass",
           "bootstrap",
           "gradle",
           "groovy",
           "grails",
           "git",
           "jquery",
           "couchdb"
       ],
       "pic": "/images/logo.png"
   },
   "education": {
       "schools": [
           {
               "name": ": <i>DEVRY UNIVERSITY</i>:",
               "location": "~TINLEY-PARK,-~[IL]LINOIS.",
               "degree": "FOR THE BACHELORS' OF THE SCIENCE.",
               "majors": [
                   ": [E]LECTRONICS-[EN]GINEERING.",
                   ": COMPUTER-[EN]GINEERING-TECHNOLOGY."
               ],
               "dates": 2009
           }
       ],
       "onlineCourses": [
           {
               "title": ": <i>INTRO TO HTML AND CSS</i>:",
               "school": ": <i>UDACITY</i>:",
               "date": 2015,
               "url": "https://www.udacity.com"
           },
           {
               "title": ": <i>RESPONSIVE WEB DESIGN FUNDAMENTALS</i>:",
               "school": ": <i>UDACITY</i>:",
               "date": 2015,
               "url": "https://www.udacity.com"
           },
           {
               "title": ": <i>RESPONSIVE IMAGES</i>:",
               "school": ": <i>UDACITY</i>:",
               "date": 2015,
               "url": "https://www.udacity.com"
           },
           {
               "title": ": <i>JAVASCRIPT BASICS</i>:",
               "school": ": <i>UDACITY</i>:",
               "date": 2015,
               "url": "https://www.udacity.com"
           },
           {
               "title": ": <i>OBJECT-ORIENTED JAVASCRIPT</i>:",
               "school": ": <i>UDACITY</i>:",
               "date": 2015,
               "url": "https://www.udacity.com"
           }
       ]
   },
   "work": {
       "jobs": [
           {
               "employer": ": FREELANCER.",
               "title": ": FREELANCER.",
               "location": ": GLOBAL.",
               "dates": "~NOVEMBER,-~2015: TILL: ~current.",
               "description": "FOR THE CONTRACTING-WORK-[ON]LY IS WITH THE VARIOUS-CLIENT-TYPES."
           },
           {
               "employer": ": <i>CRITICAL MASS</i>:",
               "title": ": <i>SENIOR APPLICATION DEVELOPER</i>:",
               "location": "~CHICAGO,-~[IL]LINOIS.",
               "dates": "~JANUARY,-~2014: TILL: ~OCTOBER,-~2015.",
               "description": "FOR THE SPECIALTY OF THE SOFTWARE-[DE]VELOPMENT IS WITH THE BACKEND-CODING OF THE GRAILS-STACK."
           },
           {
               "employer": ": <i>INNERWORKINGS</i>:",
               "title": ": <i>APPLICATION DEVELOPER</i>:",
               "location": "~CHICAGO,-~[IL]LINOIS.",
               "dates": "~JANUARY,-~2012: TILL: ~JANUARY,-~2014.",
               "description": "FOR THE SPECIALTY OF THE SOFTWARE-[DE]VELOPMENT IS WITH THE BACKEND-CODING OF THE JAVA AND: C-SHARP-LANGUAGES."
           },
           {
               "employer": ": <i>SIGNATURE CONSULTANTS</i>:",
               "title": ": <i>JUNIOR CONSULTANT</i>:",
               "location": "~CHICAGO,-~[IL]LINOIS.",
               "dates": "~AUGUST,-~2012: TILL: ~DECEMBER,-~2012.",
               "description": "FOR THE DUTIES OF THE CONSULTANCY IS WITH THE [AS]SIGNMENT OF THE <i>INNERWORKINGS</i>."
           },
           {
               "employer": ": <i>SYCLO</i>:",
               "title": ": <i>PRODUCT DEVELOPER</i>:",
               "location": "~HOFFMAN-[ES]TATES,-~[IL]LINOIS.",
               "dates": "~AUGUST,-~2011: TILL: ~AUGUST,-~2012.",
               "description": "FOR THE DUTIES OF THE SOFTWARE-[DE]VELOPMENT IS WITH THE TOOLS OF THE <i>IBM</i>'S-MAXIMO AND: TRIRIGA-PRODUCTS."
           },
           {
               "employer": ": <i>POLARIS LABORATORIES</i>:",
               "title": ": <i>JUNIOR SOFTWARE ENGINEER</i>:",
               "location": "~[IN]DIANAPOLIS,-~[IN]DIANA.",
               "dates": "~FEBRUARY,-~2010: TILL: ~MARCH/2011.",
               "description": "FOR THE NEW-[DE]SIGN OF THE LEGACY-PLATFORM IS WITH THE CONVERSION OF THE COLD-FUSION WITH THE JAVA."
           }
       ]
   },
   "projects": [
       {
           "title": ": DYNAMIC-WORK-HISTORY.",
           "dates": 2015,
           "description": "FOR THE SHOWCASING OF THE PAST-JOBS.",
           "images": [
               "/images/logo.png"
           ]
       }
   ]
}