package com.mobilecapptivate.translator.web.rest;

import com.mobilecapptivate.translator.TranslatorApp;

import com.mobilecapptivate.translator.domain.QuantumDocument;
import com.mobilecapptivate.translator.repository.QuantumDocumentRepository;
import com.mobilecapptivate.translator.service.QuantumDocumentService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the QuantumDocumentResource REST controller.
 *
 * @see QuantumDocumentResource
 */
@RunWith(SpringRunner.class)

@SpringBootTest(classes = TranslatorApp.class)

public class QuantumDocumentResourceIntTest {
    private static final String DEFAULT_USER = "AAAAA";
    private static final String UPDATED_USER = "BBBBB";
    private static final String DEFAULT_TEXT = "AAAAA";
    private static final String UPDATED_TEXT = "BBBBB";
    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";
    private static final List<String> DEFAULT_AUTOGRAPHERS = Arrays.<String>asList("AAAAA");
    private static final List<String> UPDATED_AUTOGRAPHERS = Arrays.<String>asList("BBBBB");
    private static final String DEFAULT_FLAG = "AAAAA";
    private static final String UPDATED_FLAG = "BBBBB";

    @Inject
    private QuantumDocumentRepository quantumDocumentRepository;

    @Inject
    private QuantumDocumentService quantumDocumentService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restQuantumDocumentMockMvc;

    private QuantumDocument quantumDocument;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        QuantumDocumentResource quantumDocumentResource = new QuantumDocumentResource();
        ReflectionTestUtils.setField(quantumDocumentResource, "quantumDocumentService", quantumDocumentService);
        this.restQuantumDocumentMockMvc = MockMvcBuilders.standaloneSetup(quantumDocumentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static QuantumDocument createEntity() {
        QuantumDocument quantumDocument = new QuantumDocument()
                .user(DEFAULT_USER)
                .text(DEFAULT_TEXT)
                .title(DEFAULT_TITLE)
                .autographers(DEFAULT_AUTOGRAPHERS)
                .flag(DEFAULT_FLAG);
        return quantumDocument;
    }

    @Before
    public void initTest() {
        quantumDocumentRepository.deleteAll();
        quantumDocument = createEntity();
    }

    @Test
    public void createQuantumDocument() throws Exception {
        int databaseSizeBeforeCreate = quantumDocumentRepository.findAll().size();

        // Create the QuantumDocument

        restQuantumDocumentMockMvc.perform(post("/api/quantum-documents")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(quantumDocument)))
                .andExpect(status().isCreated());

        // Validate the QuantumDocument in the database
        List<QuantumDocument> quantumDocuments = quantumDocumentRepository.findAll();
        assertThat(quantumDocuments).hasSize(databaseSizeBeforeCreate + 1);
        QuantumDocument testQuantumDocument = quantumDocuments.get(quantumDocuments.size() - 1);
        assertThat(testQuantumDocument.getUser()).isEqualTo(DEFAULT_USER);
        assertThat(testQuantumDocument.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testQuantumDocument.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testQuantumDocument.getAutographers()).isEqualTo(DEFAULT_AUTOGRAPHERS);
        assertThat(testQuantumDocument.getFlag()).isEqualTo(DEFAULT_FLAG);
    }

    @Test
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = quantumDocumentRepository.findAll().size();
        // set the field null
        quantumDocument.setTitle(null);

        // Create the QuantumDocument, which fails.

        restQuantumDocumentMockMvc.perform(post("/api/quantum-documents")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(quantumDocument)))
                .andExpect(status().isBadRequest());

        List<QuantumDocument> quantumDocuments = quantumDocumentRepository.findAll();
        assertThat(quantumDocuments).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkFlagIsRequired() throws Exception {
        int databaseSizeBeforeTest = quantumDocumentRepository.findAll().size();
        // set the field null
        quantumDocument.setFlag(null);

        // Create the QuantumDocument, which fails.

        restQuantumDocumentMockMvc.perform(post("/api/quantum-documents")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(quantumDocument)))
                .andExpect(status().isBadRequest());

        List<QuantumDocument> quantumDocuments = quantumDocumentRepository.findAll();
        assertThat(quantumDocuments).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllQuantumDocuments() throws Exception {
        // Initialize the database
        quantumDocumentRepository.save(quantumDocument);

        // Get all the quantumDocuments
        restQuantumDocumentMockMvc.perform(get("/api/quantum-documents?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(quantumDocument.getId())))
                .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())))
                .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].autographers").value(hasItem(DEFAULT_AUTOGRAPHERS.toString())))
                .andExpect(jsonPath("$.[*].flag").value(hasItem(DEFAULT_FLAG.toString())));
    }

    @Test
    public void getQuantumDocument() throws Exception {
        // Initialize the database
        quantumDocumentRepository.save(quantumDocument);

        // Get the quantumDocument
        restQuantumDocumentMockMvc.perform(get("/api/quantum-documents/{id}", quantumDocument.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(quantumDocument.getId()))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.autographers").value(DEFAULT_AUTOGRAPHERS.toString()))
            .andExpect(jsonPath("$.flag").value(DEFAULT_FLAG.toString()));
    }

    @Test
    public void getNonExistingQuantumDocument() throws Exception {
        // Get the quantumDocument
        restQuantumDocumentMockMvc.perform(get("/api/quantum-documents/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateQuantumDocument() throws Exception {
        // Initialize the database
        quantumDocumentService.save(quantumDocument);

        int databaseSizeBeforeUpdate = quantumDocumentRepository.findAll().size();

        // Update the quantumDocument
        QuantumDocument updatedQuantumDocument = quantumDocumentRepository.findOne(quantumDocument.getId());
        updatedQuantumDocument
                .user(UPDATED_USER)
                .text(UPDATED_TEXT)
                .title(UPDATED_TITLE)
                .autographers(UPDATED_AUTOGRAPHERS)
                .flag(UPDATED_FLAG);

        restQuantumDocumentMockMvc.perform(put("/api/quantum-documents")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedQuantumDocument)))
                .andExpect(status().isOk());

        // Validate the QuantumDocument in the database
        List<QuantumDocument> quantumDocuments = quantumDocumentRepository.findAll();
        assertThat(quantumDocuments).hasSize(databaseSizeBeforeUpdate);
        QuantumDocument testQuantumDocument = quantumDocuments.get(quantumDocuments.size() - 1);
        assertThat(testQuantumDocument.getUser()).isEqualTo(UPDATED_USER);
        assertThat(testQuantumDocument.getText()).isEqualTo(UPDATED_TEXT);
        assertThat(testQuantumDocument.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testQuantumDocument.getAutographers()).isEqualTo(UPDATED_AUTOGRAPHERS);
        assertThat(testQuantumDocument.getFlag()).isEqualTo(UPDATED_FLAG);
    }

    @Test
    public void deleteQuantumDocument() throws Exception {
        // Initialize the database
        quantumDocumentService.save(quantumDocument);

        int databaseSizeBeforeDelete = quantumDocumentRepository.findAll().size();

        // Get the quantumDocument
        restQuantumDocumentMockMvc.perform(delete("/api/quantum-documents/{id}", quantumDocument.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<QuantumDocument> quantumDocuments = quantumDocumentRepository.findAll();
        assertThat(quantumDocuments).hasSize(databaseSizeBeforeDelete - 1);
    }
}
