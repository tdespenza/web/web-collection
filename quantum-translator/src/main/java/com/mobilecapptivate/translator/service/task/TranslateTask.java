package com.mobilecapptivate.translator.service.task;

import com.mobilecapptivate.translator.service.util.TranslateAdapter;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Desspenza ON THE ~10/13/16.
 */
public class TranslateTask {
    private String html;
    private AsyncRestTemplate asyncRestTemplate;
    private RestTemplate restTemplate;
    private URI uri;

    public TranslateTask(final String html,
                         final AsyncRestTemplate asyncRestTemplate,
                         final URI uri) {
        this.html = html;
        this.asyncRestTemplate = asyncRestTemplate;
        this.uri = uri;
    }

    public TranslateTask(final String html,
                         final RestTemplate restTemplate,
                         final URI uri) {
        this.html = html;
        this.restTemplate = restTemplate;
        this.uri = uri;
    }

    public ListenableFuture<String> translateAsync() {
        final String text = StringEscapeUtils.escapeHtml4(html);

        if (StringUtils.isBlank(text)) return new AsyncResult<>(StringUtils.EMPTY);

        final MultiValueMap<String, String> form = new LinkedMultiValueMap<>(1);
        form.add("text", text);

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        return translatedTextAsync(uri, form, headers);
    }

    public String translate() {
        final String text = StringEscapeUtils.escapeHtml4(html);

        if (StringUtils.isBlank(text)) return StringUtils.EMPTY;

        final MultiValueMap<String, String> form = new LinkedMultiValueMap<>(1);
        form.add("text", text);

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        return translatedText(uri, form, headers);
    }

    private ListenableFuture<String> translatedTextAsync(final URI uri,
                                                         final MultiValueMap<String, String> form,
                                                         final HttpHeaders headers) {
        return new TranslateAdapter(asyncRestTemplate.postForEntity(uri, new HttpEntity<Object>(form, headers), String.class));
    }

    private String translatedText(final URI uri,
                                  final MultiValueMap<String, String> form,
                                  final HttpHeaders headers) {
        return restTemplate.postForObject(uri, new HttpEntity<Object>(form, headers), String.class);
    }
}
