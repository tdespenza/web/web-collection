package com.mobilecapptivate.translator.service.dto;

import java.io.Serializable;

/**
 * Created by tdespenza on 10/6/16.
 */
public class ExportDTO implements Serializable {
    private String filename;
    private String fileUrl;

    public ExportDTO(final String filename, final String fileUrl) {
        this.filename = filename;
        this.fileUrl = fileUrl;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(final String filename) {
        this.filename = filename;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(final String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
