package com.mobilecapptivate.translator.service.dto;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * Created by tdespenza on 8/27/16.
 */
public class LanguageDTO implements Serializable, Comparable {
    private String code;
    private String name;

    public LanguageDTO(final String code, final String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public int compareTo(final Object object) {
        final LanguageDTO that = (LanguageDTO) object;

        return new CompareToBuilder().append(this.name, that.name).toComparison();
    }
}
