package com.mobilecapptivate.translator.service.dto;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by tdespenza on 10/4/16.
 */
public class QuantumDocumentDTO implements Serializable {
    private String title;
    private String format;
    private String imageUrl;
    private String code;
    private Map<String, String> text;

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(final String format) {
        this.format = format;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public Map<String, String> getText() {
        return text;
    }

    public void setText(final Map<String, String> text) {
        this.text = text;
    }
}
