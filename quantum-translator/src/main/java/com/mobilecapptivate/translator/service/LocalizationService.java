package com.mobilecapptivate.translator.service;

import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.Locale;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~10/10/16.
 */
@Service
public class LocalizationService {

    @Inject
    private MessageSource messageSource;

    @Nonnull
    public String getValue(final String key, final String code) throws NoSuchMessageException {
        return messageSource.getMessage(key, null, null, Locale.forLanguageTag(code));
    }
}
