package com.mobilecapptivate.translator.service;

import com.mobilecapptivate.translator.domain.QuantumDocument;
import com.mobilecapptivate.translator.repository.QuantumDocumentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Service Implementation for managing QuantumDocument.
 */
@Service
public class QuantumDocumentService {

    private final Logger log = LoggerFactory.getLogger(QuantumDocumentService.class);
    
    @Inject
    private QuantumDocumentRepository quantumDocumentRepository;

    /**
     * Save a quantumDocument.
     *
     * @param quantumDocument the entity to save
     * @return the persisted entity
     */
    public QuantumDocument save(QuantumDocument quantumDocument) {
        log.debug("Request to save QuantumDocument : {}", quantumDocument);
        QuantumDocument result = quantumDocumentRepository.save(quantumDocument);
        return result;
    }

    /**
     *  Get all the quantumDocuments.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    public Page<QuantumDocument> findAll(Pageable pageable) {
        log.debug("Request to get all QuantumDocuments");
        Page<QuantumDocument> result = quantumDocumentRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one quantumDocument by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    public QuantumDocument findOne(String id) {
        log.debug("Request to get QuantumDocument : {}", id);
        QuantumDocument quantumDocument = quantumDocumentRepository.findOne(id);
        return quantumDocument;
    }

    /**
     *  Delete the  quantumDocument by id.
     *
     *  @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete QuantumDocument : {}", id);
        quantumDocumentRepository.delete(id);
    }
}
