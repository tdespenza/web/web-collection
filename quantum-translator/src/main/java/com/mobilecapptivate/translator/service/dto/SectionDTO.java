package com.mobilecapptivate.translator.service.dto;

import javax.annotation.Nonnull;
import java.io.Serializable;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Desspenza ON THE ~10/12/16.
 */
public class SectionDTO implements Serializable {
    private String toCode;
    private String fromCode;
    private String text;

    public SectionDTO() {

    }

    public SectionDTO(final String fromCode, final @Nonnull String toCode, final @Nonnull String text) {
        this.toCode = toCode;
        this.fromCode = fromCode;
        this.text = text;
    }

    public String getToCode() {
        return toCode;
    }

    public void setToCode(final String toCode) {
        this.toCode = toCode;
    }

    public String getFromCode() {
        return fromCode;
    }

    public void setFromCode(final String fromCode) {
        this.fromCode = fromCode;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }
}
