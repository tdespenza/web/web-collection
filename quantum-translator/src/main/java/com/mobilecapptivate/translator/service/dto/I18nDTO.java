package com.mobilecapptivate.translator.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by tdespenza on 8/27/16.
 */
public class I18nDTO implements Serializable {
    @JsonProperty("dirs")
    private String[] directions;

    @JsonProperty("langs")
    private Map<String, String> languages;

    public String[] getDirections() {
        return directions;
    }

    public void setDirections(final String[] directions) {
        this.directions = directions;
    }

    public Map<String, String> getLanguages() {
        return languages;
    }

    public void setLanguages(final Map<String, String> languages) {
        this.languages = languages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof I18nDTO)) return false;

        I18nDTO that = (I18nDTO) o;

        return new EqualsBuilder()
            .append(directions, that.directions)
            .append(languages, that.languages)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(directions)
            .append(languages)
            .toHashCode();
    }

    @Override
    public String toString() {
        return "I18nDTO{" +
            "directions=" + Arrays.toString(directions) +
            ", languages=" + languages +
            '}';
    }
}
