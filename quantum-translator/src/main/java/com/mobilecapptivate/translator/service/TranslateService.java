package com.mobilecapptivate.translator.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.mobilecapptivate.translator.config.CustomConfiguration;
import com.mobilecapptivate.translator.service.dto.I18nDTO;
import com.mobilecapptivate.translator.service.dto.LanguageDTO;
import com.mobilecapptivate.translator.service.dto.TranslateDTO;
import com.mobilecapptivate.translator.service.task.TranslateTask;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.concurrent.ExecutionException;

@Service
public class TranslateService {
    protected static final Logger log = LoggerFactory.getLogger(TranslateService.class);
    private AsyncRestTemplate asyncRestTemplate;
    private RestTemplate restTemplate;
    private CustomConfiguration customConfiguration;
    private static final String ENDPOINT = "https://translate.yandex.net/api/v1.5/tr.json";

    @Inject
    public TranslateService(final CustomConfiguration customConfiguration) {
        this.customConfiguration = customConfiguration;
        this.asyncRestTemplate = new AsyncRestTemplate();
        this.restTemplate = new RestTemplateBuilder().build();
    }

    @Nonnull
    @Async
    public ListenableFuture<TranslateDTO> translateAllAsync(Optional<String> from, final String to, final TranslateDTO translate)
        throws JsonProcessingException, ExecutionException, InterruptedException {
        if (!from.isPresent() || StringUtils.isBlank(from.get())) {
            from = Optional.of(JsonPath.read(detect(translate.getHeader()), "$.lang"));
            if (!from.isPresent()) from = Optional.of(JsonPath.read(detect(translate.getTerms()), "$.lang"));
            if (!from.isPresent()) from = Optional.of(JsonPath.read(detect(translate.getFacts()), "$.lang"));
            if (!from.isPresent()) from = Optional.of(JsonPath.read(detect(translate.getAutograph()), "$.lang"));
        }

        final TranslateDTO translated = new TranslateDTO(from.get(), to);
        if (!from.isPresent()) return new AsyncResult<>(translated);

        final ListenableFuture<String> header = translateSectionAsync(from, to, translate.getHeader());
        final ListenableFuture<String> terms = translateSectionAsync(from, to, translate.getTerms());
        final ListenableFuture<String> facts = translateSectionAsync(from, to, translate.getFacts());
        final ListenableFuture<String> autograph = translateSectionAsync(from, to, translate.getAutograph());

        translated.setHeader(header.get());
        translated.setTerms(terms.get());
        translated.setFacts(facts.get());
        translated.setAutograph(autograph.get());

        return new AsyncResult<>(translated);
    }

    @Nonnull
    @Async
    public ListenableFuture<String> translateSectionAsync(Optional<String> from, final String to, final String text)
        throws JsonProcessingException, ExecutionException, InterruptedException {
        if (!from.isPresent() || StringUtils.isBlank(from.get())) {
            from = Optional.of(JsonPath.read(detect(text), "$.lang"));
        }

        final String key = customConfiguration.getYandex().getKey();
        final URI uri = UriComponentsBuilder.fromHttpUrl(ENDPOINT)
            .path("/translate")
            .queryParam("lang", from.get() + "-" + to)
            .queryParam("key", key)
            .queryParam("format", "html")
            .build().encode().toUri();

        return new TranslateTask(text, asyncRestTemplate, uri).translateAsync();
    }

    @Nonnull
    public String translateSection(Optional<String> from, final String to, final String text)
        throws JsonProcessingException, ExecutionException, InterruptedException {
        if (!from.isPresent() || StringUtils.isBlank(from.get())) {
            from = Optional.of(JsonPath.read(detect(text), "$.lang"));
        }

        final URI uri = buildTranslateUri(from.get(), to);
        return new TranslateTask(text, restTemplate, uri).translate();
    }

    public String detect(final @Nonnull String text) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        final MultiValueMap<String, String> form = new LinkedMultiValueMap<>(1);
        form.add("text", StringEscapeUtils.escapeHtml4(text));

        final HttpEntity<Object> entity = new HttpEntity<>(form, headers);
        return restTemplate.postForObject(buildDetectUri(), entity, String.class);
    }

    public String getAllLanguages() throws IOException {
        final String key = customConfiguration.getYandex().getKey();
        final String url = ENDPOINT + "/getLangs?key=" + key + "&ui=en";
        final String json = restTemplate.getForObject(url, String.class);
        final ObjectMapper mapper = new ObjectMapper();
        final I18nDTO languages = mapper.readValue(json, I18nDTO.class);
        final Set<LanguageDTO> converted = convertToLanguageSet(languages.getLanguages());

        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(converted);
    }

    private URI buildTranslateUri(final String from, final String to) {
        final String key = customConfiguration.getYandex().getKey();
        return UriComponentsBuilder.fromHttpUrl(ENDPOINT)
            .path("/translate")
            .queryParam("lang", from + "-" + to)
            .queryParam("key", key)
            .queryParam("format", "html")
            .build().encode().toUri();
    }

    private URI buildDetectUri() {
        final String key = customConfiguration.getYandex().getKey();
        return UriComponentsBuilder.fromHttpUrl(ENDPOINT)
            .path("/detect")
            .queryParam("hint", "en")
            .queryParam("key", key)
            .build().encode().toUri();
    }

    @Nonnull
    private Set<LanguageDTO> convertToLanguageSet(final Map<String, String> from) {
        final SortedSet<LanguageDTO> to = new TreeSet<>();

        from.keySet().stream().forEach(code -> to.add(new LanguageDTO(code, from.get(code))));

        return to;
    }

    /*private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(final Map<K, V> unsortMap) {
        final List<Map.Entry<K, V>> list = new LinkedList<>(unsortMap.entrySet());
        final Map<K, V> result = new LinkedHashMap<>(list.size());

        Collections.sort(list, (o1, o2) -> (o1.getValue()).compareTo(o2.getValue()));

        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }*/
}
