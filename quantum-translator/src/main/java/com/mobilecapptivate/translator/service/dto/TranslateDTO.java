package com.mobilecapptivate.translator.service.dto;

import javax.annotation.Nonnull;
import java.io.Serializable;

/**
 * Created by tdespenza on 10/2/16.
 */
public class TranslateDTO implements Serializable {

    @Nonnull
    private String toCode;
    private String fromCode;
    private String header;
    private String terms;
    private String facts;
    private String autograph;

    public TranslateDTO() {
    }

    public TranslateDTO(final String fromCode, final String toCode) {
        this.toCode = toCode;
        this.fromCode = fromCode;
    }

    public String getToCode() {
        return toCode;
    }

    public void setToCode(final String toCode) {
        this.toCode = toCode;
    }

    public String getFromCode() {
        return fromCode;
    }

    public void setFromCode(final String fromCode) {
        this.fromCode = fromCode;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(final String header) {
        this.header = header;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(final String terms) {
        this.terms = terms;
    }

    public String getFacts() {
        return facts;
    }

    public void setFacts(final String facts) {
        this.facts = facts;
    }

    public String getAutograph() {
        return autograph;
    }

    public void setAutograph(final String autograph) {
        this.autograph = autograph;
    }

    @Override
    public String toString() {
        return "TranslateDTO{" +
            "toCode='" + toCode + '\'' +
            ", fromCode='" + fromCode + '\'' +
            ", header='" + header + '\'' +
            ", terms='" + terms + '\'' +
            ", facts='" + facts + '\'' +
            ", autograph='" + autograph + '\'' +
            '}';
    }
}
