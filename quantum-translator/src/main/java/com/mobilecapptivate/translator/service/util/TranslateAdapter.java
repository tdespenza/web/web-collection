package com.mobilecapptivate.translator.service.util;

import com.jayway.jsonpath.JsonPath;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureAdapter;

import java.util.concurrent.ExecutionException;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~10/13/16.
 */
public class TranslateAdapter extends ListenableFutureAdapter<String, ResponseEntity<String>> {

    public TranslateAdapter(final ListenableFuture<ResponseEntity<String>> listenableFuture) {
        super(listenableFuture);
    }

    @Override
    protected String adapt(final ResponseEntity<String> responseEntity) throws ExecutionException {
        return StringEscapeUtils.unescapeHtml4(
            removeQuote(JsonPath.read(responseEntity.getBody(), "$.text[0]"))
        );
    }

    private String removeQuote(final String text) {
        return text.replaceAll("\"", "");
    }
}
