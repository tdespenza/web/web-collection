package com.mobilecapptivate.translator.exporter;

import com.mobilecapptivate.translator.service.dto.ExportDTO;
import com.mobilecapptivate.translator.service.dto.QuantumDocumentDTO;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageMar;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by tdespenza on 9/25/16.
 */
@Component
public class WordExporter extends DocumentExporter {
    private final String WORD = "word";
    private final String FONT_FAMILY = "Courier New";

    @Override
    public ExportDTO export(final QuantumDocumentDTO quantumDocument, final String baseUrl) {
        final XWPFDocument document = new XWPFDocument();
        final String docLocation = String.format(DOC_PATH, WORD, quantumDocument.getTitle());
        final Path docPath = Paths.get(docLocation).getParent();
        final String imageLocation = String.format(IMAGE_PATH, JPG, quantumDocument.getTitle());
        final Path imagePath = Paths.get(imageLocation).getParent();

        createPath(docPath);
        createPath(imagePath);
        setMargins(document);

        try (final OutputStream outputStream = Files.newOutputStream(Paths.get(docLocation))) {
            createHeader(document, baseUrl, quantumDocument);
            createBody(document, quantumDocument);
            createFooter(document, getValue(FOOTER_KEY, quantumDocument.getCode()));
            document.write(outputStream);
            document.close();

            return new ExportDTO(
                FilenameUtils.getBaseName(quantumDocument.getTitle()),
                Paths.get(docLocation).toUri().toURL().toExternalForm()
            );

        } catch (final IOException | InvalidFormatException | TranscoderException | URISyntaxException e) {
            log.error("Error exporting Word document", e);
            return null;
        }
    }

    @Override
    public byte[] getFile(final String file) {
        final String fileLocation = String.format(DOC_PATH, WORD, file);
        final Path path = Paths.get(fileLocation);
        byte[] data = null;

        if (Files.exists(path)) {
            try {
                data = Files.readAllBytes(path);

            } catch (final IOException e) {
                log.error("Error getting file", e);
            }
        }

        return data;
    }

    private void setMargins(final XWPFDocument document) {
        final CTSectPr marginSection = document.getDocument().getBody().addNewSectPr();
        final CTPageMar margin = marginSection.addNewPgMar();
        margin.setLeft(BigInteger.valueOf(720L));
        margin.setTop(BigInteger.valueOf(720L));
        margin.setRight(BigInteger.valueOf(720L));
        margin.setBottom(BigInteger.valueOf(720L));
    }

    private void createHeader(final XWPFDocument document, final String baseUrl, final QuantumDocumentDTO quantumDocument)
        throws URISyntaxException, IOException, TranscoderException, InvalidFormatException {
        final String svgPath = String.format(SVG_PATH, baseUrl, quantumDocument.getImageUrl());
        final String filename = FilenameUtils.getBaseName(quantumDocument.getTitle()) + ".jpg";
        final String imagePath = String.format(IMAGE_PATH, JPG, filename);
        convertToJpeg(svgPath, imagePath);

        final XWPFParagraph flagParagraph = document.createParagraph();
        flagParagraph.setAlignment(ParagraphAlignment.BOTH);

        final XWPFRun flag = flagParagraph.createRun();
        flag.addPicture(
            Files.newInputStream(Paths.get(imagePath)),
            XWPFDocument.PICTURE_TYPE_JPEG,
            FLAG_NAME, Units.toEMU(35), Units.toEMU(25)
        );

        final XWPFParagraph venueParagraph = document.createParagraph();
        venueParagraph.setAlignment(ParagraphAlignment.BOTH);

        final XWPFRun venue = venueParagraph.createRun();
        venue.addCarriageReturn();
        venue.setFontFamily(FONT_FAMILY);
        venue.setBold(true);
        venue.setUnderline(UnderlinePatterns.SINGLE);
        venue.setFontSize(8);
        venue.setText(getValue(COURT_VENUE, quantumDocument.getCode()));
        venue.addCarriageReturn();
        venue.addCarriageReturn();
    }

    private void createBody(final XWPFDocument document, final QuantumDocumentDTO quantumDocument) {
        quantumDocument
            .getText()
            .entrySet()
            .parallelStream()
            .filter(map -> map.getValue() != null)
            .forEachOrdered(section -> {
                if ("autograph".equals(section.getKey())) {
                    final XWPFRun seal = document.createParagraph().createRun();
                    seal.setFontFamily(FONT_FAMILY);
                    addTabs(seal, 35);
                    seal.setText(SEAL);

                    final XWPFRun underline = document.createParagraph().createRun();
                    underline.setUnderline(UnderlinePatterns.SINGLE);
                    seal.setFontFamily(FONT_FAMILY);
                    underline.setText(": ");
                    addTabs(underline, 150);
                    underline.setText(".");
                }

                final Stream<String> paragraphs = Arrays.stream(section.getValue().split("\n"));
                paragraphs.forEachOrdered(paragraph -> createParagraph(document, paragraph));
            });
    }

    private void createParagraph(final XWPFDocument document, final String text) {
        final XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.BOTH);

        final XWPFRun run = paragraph.createRun();
        run.setFontFamily(FONT_FAMILY);
        run.setFontSize(11);
        run.setText(text.replaceAll("\\<.*?>", ""));
    }

    private void createFooter(final XWPFDocument document, final String text) throws IOException {
        final XWPFParagraph paragraph = document.createHeaderFooterPolicy()
            .createFooter(XWPFHeaderFooterPolicy.DEFAULT)
            .createParagraph();
        paragraph.setAlignment(ParagraphAlignment.BOTH);

        final XWPFRun run = paragraph.createRun();
        run.setFontFamily(FONT_FAMILY);
        run.setFontSize(8);
        run.setText(text);
    }

    private void addTabs(final XWPFRun run, final int tabs) {
        for (int index = 0; index < tabs; index++) {
            run.addTab();
        }
    }

    private void convertToJpeg(final String svgPath, final String imagePath) throws TranscoderException, IOException, URISyntaxException {
        final JPEGTranscoder transcoder = new JPEGTranscoder();
        transcoder.addTranscodingHint(JPEGTranscoder.KEY_QUALITY, new Float(1));
        transcoder.addTranscodingHint(JPEGTranscoder.KEY_WIDTH, new Float(140));
        transcoder.addTranscodingHint(JPEGTranscoder.KEY_HEIGHT, new Float(100));

        try (final FileOutputStream outputStream = new FileOutputStream(new File(imagePath))) {
            final TranscoderInput input = new TranscoderInput(new URI(svgPath).toString());
            final TranscoderOutput output = new TranscoderOutput(outputStream);
            transcoder.transcode(input, output);

        } catch (final IOException e) {
            throw e;
        }
    }
}
