package com.mobilecapptivate.translator.exporter;

import com.mobilecapptivate.translator.service.dto.ExportDTO;
import com.mobilecapptivate.translator.service.dto.QuantumDocumentDTO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created by tdespenza on 9/25/16.
 */
@Component
public class PdfExporter extends DocumentExporter {
    private static final Logger log = LoggerFactory.getLogger(PdfExporter.class);

    @Override
    public ExportDTO export(final QuantumDocumentDTO quantumDocument, String baseUrl) {
        final PDDocument document = new PDDocument();
        final String saveLocation = String.format(DOC_PATH, "pdf", quantumDocument.getTitle());
        final Path filePath = Paths.get(saveLocation).getParent();

        createPath(filePath);

        try (final OutputStream outputStream = Files.newOutputStream(Paths.get(saveLocation))) {
            final Map<String, String> sections = quantumDocument.getText();

            sections.entrySet().stream().forEachOrdered(section -> {
                final Stream<String> paragraphs = Arrays.stream(section.getValue().split("\n"));

                paragraphs.forEachOrdered(paragraph -> {
                    try {
                        document.addPage(createPage(document, paragraph));

                    } catch (final IOException e) {
                        log.error("Error creating PDF page", e);
                    }
                });
            });

            document.save(outputStream);
            document.close();

            return new ExportDTO(quantumDocument.getTitle() , saveLocation);

        } catch (final IOException e) {
            log.error("Error exporting PDF document", e);
            return null;
        }
    }

    @Override
    public byte[] getFile(final String file) {
        return new byte[0];
    }

    private PDPage createPage(final PDDocument document, final String text) throws IOException {
        final PDPage page = new PDPage();
        final PDPageContentStream contentStream = new PDPageContentStream(document, page);
        final int lineLength = 700 - 25;
        int remainingText = text.length();
        String line;

        contentStream.beginText();
        contentStream.newLineAtOffset(25, 700);

        while (remainingText > 0) {
            line = text.substring(text.length() - remainingText, remainingText - lineLength);
            contentStream.showText(line);
            remainingText -= lineLength;
        }

        contentStream.endText();
        contentStream.close();

        return page;
    }
}
