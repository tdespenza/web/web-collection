package com.mobilecapptivate.translator.exporter;

import com.mobilecapptivate.translator.service.dto.ExportDTO;
import com.mobilecapptivate.translator.service.dto.QuantumDocumentDTO;
import org.springframework.stereotype.Component;

/**
 * Created by tdespenza on 9/25/16.
 */
@Component
public class PlainTextExporter extends DocumentExporter {
    @Override
    public ExportDTO export(final QuantumDocumentDTO quantumDocument, String baseUrl) {
        final String saveLocation = String.format(DOC_PATH, "txt", quantumDocument.getTitle());

        return new ExportDTO(quantumDocument.getTitle() , saveLocation);
    }

    @Override
    public byte[] getFile(String file) {
        return new byte[0];
    }
}
