package com.mobilecapptivate.translator.exporter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mobilecapptivate.translator.service.LocalizationService;
import com.mobilecapptivate.translator.service.TranslateService;
import com.mobilecapptivate.translator.service.dto.ExportDTO;
import com.mobilecapptivate.translator.service.dto.QuantumDocumentDTO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.NoSuchMessageException;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Desspenza ON THE ~10/13/16.
 */
public abstract class DocumentExporter {
    protected static final Logger log = LoggerFactory.getLogger(DocumentExporter.class);
    protected static final String DOC_PATH = "src/main/webapp/docs/%s/%s";
    protected static final String IMAGE_PATH = "src/main/webapp/images/%s/%s";
    protected static final String SVG_PATH = "%s/%s";
    protected static final String SEAL = ": SEAL.";
    protected static final String FLAG_NAME = "flag.jpg";
    protected static final String JPG = "jpg";
    protected static final String FOOTER_KEY = "export.footer.copyclaim";
    protected static final String COURT_VENUE = "export.header.venue";

    @Inject
    protected LocalizationService localizationService;

    @Inject
    protected TranslateService translateService;

    public abstract ExportDTO export(final QuantumDocumentDTO quantumDocument, final String baseUrl);

    public abstract byte[] getFile(final String file);

    protected void createPath(final @Nonnull Path path) {
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (final IOException e) {
                log.error("Error creating export path", e);
            }
        }
    }

    @Nonnull
    protected String getValue(final String key, final String code) throws JsonProcessingException {
        try {
            String footer = localizationService.getValue(key, code);

            if (StringUtils.isNotEmpty(footer)) {
                final String detected = translateService.detect(footer);

                if (StringUtils.isNotBlank(detected) && code.equals(detected)) {
                    return footer;
                }

                footer = translateService.translateSection(Optional.empty(), code, footer);
            }

            return footer;

        } catch (final NoSuchMessageException | ExecutionException | InterruptedException e) {
            log.info("reverting to default locale");
            return StringUtils.EMPTY;
        }
    }
}
