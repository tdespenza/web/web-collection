package com.mobilecapptivate.translator.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A QuantumDocument.
 */

@Document(collection = "quantum_document")
public class QuantumDocument implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("user")
    private String user;

    @NotNull
    @Field("text")
    private String text;

    @NotNull
    @Field("title")
    private String title;

    @NotNull
    @Field("autographers")
    private List<String> autographers;

    @NotNull
    @Field("flag")
    private String flag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public QuantumDocument user(String user) {
        this.user = user;
        return this;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public QuantumDocument text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public QuantumDocument title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAutographers() {
        return autographers;
    }

    public QuantumDocument autographers(List<String> autographers) {
        this.autographers = autographers;
        return this;
    }

    public void setAutographers(List<String> autographers) {
        this.autographers = autographers;
    }

    public String getFlag() {
        return flag;
    }

    public QuantumDocument flag(String flag) {
        this.flag = flag;
        return this;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        QuantumDocument quantumDocument = (QuantumDocument) o;
        if(quantumDocument.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, quantumDocument.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "QuantumDocument{" +
            "id=" + id +
            ", user='" + user + "'" +
            ", text='" + text + "'" +
            ", title='" + title + "'" +
            ", autographers='" + autographers + "'" +
            ", flag='" + flag + "'" +
            '}';
    }
}
