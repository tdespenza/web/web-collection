/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mobilecapptivate.translator.web.rest.vm;
