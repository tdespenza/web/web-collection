package com.mobilecapptivate.translator.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.mobilecapptivate.translator.security.AuthoritiesConstants;
import com.mobilecapptivate.translator.service.TranslateService;
import com.mobilecapptivate.translator.service.dto.TranslateDTO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Desspenza ON THE ~10/13/16.
 */
@RestController
@RequestMapping("/api")
public class TranslateResource {

    private final Logger log = LoggerFactory.getLogger(TranslateResource.class);

    @Inject
    private TranslateService translateService;

    @RequestMapping(value = "/translate/all",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        headers = {
            "accept=application/json",
            "content-type=application/json"
        })
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public DeferredResult<ResponseEntity<?>> translateAll(final @Valid @RequestBody TranslateDTO translate) {
        if (isBlank(translate)) {
            final DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();
            result.setErrorResult(ResponseEntity.badRequest().body("no text to translate"));
            return result;
        }

        if (StringUtils.isBlank(translate.getToCode())) {
            final DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();
            result.setErrorResult(ResponseEntity.badRequest().body("must specify language to translate to"));
            return result;
        }

        final String fromCode = translate.getFromCode();
        final String toCode = translate.getToCode();
        final TranslatableFuture<TranslateDTO> future = new TranslatableFuture<>();

        try {
            final ListenableFuture<TranslateDTO> translated = translateService.translateAllAsync(
                Optional.ofNullable(fromCode),
                toCode,
                translate);
            translated.addCallback(future);

            return future.getDeferredResult();

        } catch (final JsonProcessingException | ExecutionException | InterruptedException e) {
            log.error("error translating text", e);
            final DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();
            result.setErrorResult(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(StringUtils.EMPTY));
            return result;
        }
    }

    @RequestMapping(value = "/translate/section",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        headers = {
            "accept=application/json",
            "content-type=application/json"
        })
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public DeferredResult<ResponseEntity<?>> translateSection(final @Valid @RequestBody @Nonnull String json) {
        final Optional<String> fromCode = Optional.ofNullable(JsonPath.read(json, "$.fromCode"));
        final String toCode = JsonPath.read(json, "$.toCode");
        final String text = JsonPath.read(json, "$.text");

        if (StringUtils.isBlank(text)) {
            final DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();
            result.setErrorResult(ResponseEntity.badRequest().body("no text to translate"));
            return result;
        }

        if (StringUtils.isBlank(toCode)) {
            final DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();
            result.setErrorResult(ResponseEntity.badRequest().body("must specify language to translate to"));
            return result;
        }

        final TranslatableFuture<String> future = new TranslatableFuture<>();

        try {
            final ListenableFuture<String> translated = translateService.translateSectionAsync(fromCode, toCode, text);
            translated.addCallback(future);

            return future.getDeferredResult();

        } catch (final JsonProcessingException | ExecutionException | InterruptedException e) {
            log.error("error translating text", e);
            final DeferredResult<ResponseEntity<?>> result = new DeferredResult<>();
            result.setErrorResult(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(StringUtils.EMPTY));
            return result;
        }
    }

    @RequestMapping(value = "/languages",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<String> languages() throws IOException {
        return ResponseEntity.ok().body(translateService.getAllLanguages());
    }

    private class TranslatableFuture<T> implements ListenableFutureCallback<T> {
        final DeferredResult<ResponseEntity<?>> deferredResult = new DeferredResult<>();

        @Override
        public void onSuccess(T result) {
            ResponseEntity<String> responseEntity = null;

            try {
                responseEntity = new ResponseEntity<>(new ObjectMapper().writeValueAsString(result), HttpStatus.OK);
            } catch (final JsonProcessingException e) {
                onFailure(e);
            }

            deferredResult.setResult(responseEntity);
        }

        @Override
        public void onFailure(final Throwable throwable) {
            log.error("Failed to fetch result from remote service", throwable);
            deferredResult.setErrorResult(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        }

        public DeferredResult<ResponseEntity<?>> getDeferredResult() {
            return deferredResult;
        }
    }

    private boolean isBlank(final @NotNull TranslateDTO translate) {
        return StringUtils.isBlank(translate.getHeader())
            && StringUtils.isBlank(translate.getTerms())
            && StringUtils.isBlank(translate.getFacts())
            && StringUtils.isBlank(translate.getAutograph());
    }
}
