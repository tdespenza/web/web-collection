package com.mobilecapptivate.translator.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mobilecapptivate.translator.exporter.PdfExporter;
import com.mobilecapptivate.translator.exporter.PlainTextExporter;
import com.mobilecapptivate.translator.exporter.WordExporter;
import com.mobilecapptivate.translator.security.AuthoritiesConstants;
import com.mobilecapptivate.translator.service.UserService;
import com.mobilecapptivate.translator.service.dto.ExportDTO;
import com.mobilecapptivate.translator.service.dto.QuantumDocumentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by tdespenza on 9/26/16.
 */
@RestController
@RequestMapping("/api/export")
public class DocumentExportResource {
    private final String TITLE = "%s_%s.%s";
    private final Logger log = LoggerFactory.getLogger(DocumentExportResource.class);

    private UserService userService;
    private WordExporter wordExporter;
    private PdfExporter pdfExporter;
    private PlainTextExporter plainTextExporter;

    @Inject
    public DocumentExportResource(final UserService userService,
                                  final WordExporter wordExporter,
                                  final PdfExporter pdfExporter,
                                  final PlainTextExporter plainTextExporter) {
        this.userService = userService;
        this.wordExporter = wordExporter;
        this.pdfExporter = pdfExporter;
        this.plainTextExporter = plainTextExporter;
    }

    @RequestMapping(value = "/pdf/create",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_PDF_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<ExportDTO> createPdf(final @Valid @RequestBody QuantumDocumentDTO quantumDocument,
                                               final HttpServletRequest request) {
        quantumDocument.setTitle(String.format(
            TITLE,
            System.currentTimeMillis(),
            userService.getUserWithAuthorities().getLogin(),
            "pdf"
        ));
        return ResponseEntity.ok().body(pdfExporter.export(quantumDocument, getBaseUrl(request)));
    }

    @RequestMapping(value = "/pdf/{filename}",
        method = RequestMethod.GET)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<byte[]> getPdf(final @PathVariable String filename) {
        final String extension = ".pdf";
        final byte[] content = pdfExporter.getFile(filename + extension);
        return ResponseEntity.ok()
            .header("Content-Disposition", "inline; filename=" + filename + extension)
            .contentLength(content.length)
            .contentType(MediaType.APPLICATION_PDF)
            .body(content);
    }

    @RequestMapping(value = "/word/create",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<ExportDTO> createWordDoc(final @Valid @RequestBody QuantumDocumentDTO quantumDocument,
                                                   final HttpServletRequest request) {
        quantumDocument.setTitle(String.format(
            TITLE,
            System.currentTimeMillis(),
            userService.getUserWithAuthorities().getLogin(),
            "docx"
        ));

        return ResponseEntity.ok().body(wordExporter.export(quantumDocument, getBaseUrl(request)));
    }

    @RequestMapping(value = "/word/{filename}",
        method = RequestMethod.GET)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<byte[]> getWordDoc(final @PathVariable String filename) {
        final String extension = ".docx";
        final byte[] content = wordExporter.getFile(filename + extension);
        return ResponseEntity.ok()
            .header("Content-Disposition", "inline; filename=" + filename + extension)
            .contentLength(content.length)
            .contentType(MediaType.valueOf("application/vnd.openxmlformats-officedocument.wordprocessingml.document"))
            .body(content);
    }

    @RequestMapping(value = "/plain/create",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_XML_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<ExportDTO> createPlainText(final @Valid @RequestBody QuantumDocumentDTO quantumDocument,
                                                     final HttpServletRequest request) {
        quantumDocument.setTitle(String.format(
            TITLE,
            System.currentTimeMillis(),
            userService.getUserWithAuthorities().getLogin(),
            "txt"
        ));
        return ResponseEntity.ok().body(plainTextExporter.export(quantumDocument, getBaseUrl(request)));
    }

    @RequestMapping(value = "/plain/{filename}",
        method = RequestMethod.GET)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<byte[]> getPlainText(final @PathVariable String filename) {
        final String extension = ".txt";
        final byte[] content = pdfExporter.getFile(filename + extension);
        return ResponseEntity.ok()
            .header("Content-Disposition", "inline; filename=" + filename + extension)
            .contentLength(content.length)
            .contentType(MediaType.TEXT_PLAIN)
            .body(content);
    }

    private String getBaseUrl(final HttpServletRequest request) {
        return new StringBuilder(request.getScheme())
            .append("://")
            .append(request.getServerName())
            .append(":")
            .append(request.getServerPort())
            .toString();
    }
}
