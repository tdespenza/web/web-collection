package com.mobilecapptivate.translator.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mobilecapptivate.translator.domain.QuantumDocument;
import com.mobilecapptivate.translator.service.QuantumDocumentService;
import com.mobilecapptivate.translator.web.rest.util.HeaderUtil;
import com.mobilecapptivate.translator.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing QuantumDocument.
 */
@RestController
@RequestMapping("/api")
public class QuantumDocumentResource {

    private final Logger log = LoggerFactory.getLogger(QuantumDocumentResource.class);
        
    @Inject
    private QuantumDocumentService quantumDocumentService;

    /**
     * POST  /quantum-documents : Create a new quantumDocument.
     *
     * @param quantumDocument the quantumDocument to create
     * @return the ResponseEntity with status 201 (Created) and with body the new quantumDocument, or with status 400 (Bad Request) if the quantumDocument has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/quantum-documents",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<QuantumDocument> createQuantumDocument(@Valid @RequestBody QuantumDocument quantumDocument) throws URISyntaxException {
        log.debug("REST request to save QuantumDocument : {}", quantumDocument);
        if (quantumDocument.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("quantumDocument", "idexists", "A new quantumDocument cannot already have an ID")).body(null);
        }
        QuantumDocument result = quantumDocumentService.save(quantumDocument);
        return ResponseEntity.created(new URI("/api/quantum-documents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("quantumDocument", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /quantum-documents : Updates an existing quantumDocument.
     *
     * @param quantumDocument the quantumDocument to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated quantumDocument,
     * or with status 400 (Bad Request) if the quantumDocument is not valid,
     * or with status 500 (Internal Server Error) if the quantumDocument couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/quantum-documents",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<QuantumDocument> updateQuantumDocument(@Valid @RequestBody QuantumDocument quantumDocument) throws URISyntaxException {
        log.debug("REST request to update QuantumDocument : {}", quantumDocument);
        if (quantumDocument.getId() == null) {
            return createQuantumDocument(quantumDocument);
        }
        QuantumDocument result = quantumDocumentService.save(quantumDocument);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("quantumDocument", quantumDocument.getId().toString()))
            .body(result);
    }

    /**
     * GET  /quantum-documents : get all the quantumDocuments.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of quantumDocuments in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/quantum-documents",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<QuantumDocument>> getAllQuantumDocuments(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of QuantumDocuments");
        Page<QuantumDocument> page = quantumDocumentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/quantum-documents");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /quantum-documents/:id : get the "id" quantumDocument.
     *
     * @param id the id of the quantumDocument to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the quantumDocument, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/quantum-documents/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<QuantumDocument> getQuantumDocument(@PathVariable String id) {
        log.debug("REST request to get QuantumDocument : {}", id);
        QuantumDocument quantumDocument = quantumDocumentService.findOne(id);
        return Optional.ofNullable(quantumDocument)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /quantum-documents/:id : delete the "id" quantumDocument.
     *
     * @param id the id of the quantumDocument to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/quantum-documents/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteQuantumDocument(@PathVariable String id) {
        log.debug("REST request to delete QuantumDocument : {}", id);
        quantumDocumentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("quantumDocument", id.toString())).build();
    }

}
