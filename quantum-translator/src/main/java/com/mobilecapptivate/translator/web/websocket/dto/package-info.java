/**
 * Data Access Objects used by WebSocket services.
 */
package com.mobilecapptivate.translator.web.websocket.dto;
