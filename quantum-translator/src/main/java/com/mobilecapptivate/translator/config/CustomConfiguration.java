package com.mobilecapptivate.translator.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by tdespenza on 8/26/16.
 */
@Configuration
@ConfigurationProperties(prefix = "custom", ignoreUnknownFields = false)
public class CustomConfiguration {
    private final Yandex yandex = new Yandex();

    public Yandex getYandex() {
        return yandex;
    }

    public static class Yandex {
        private String key;

        public String getKey() {
            return key;
        }

        public void setKey(final String key) {
            this.key = key;
        }
    }
}
