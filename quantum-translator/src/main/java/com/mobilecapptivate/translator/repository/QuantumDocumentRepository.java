package com.mobilecapptivate.translator.repository;

import com.mobilecapptivate.translator.domain.QuantumDocument;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the QuantumDocument entity.
 */
@SuppressWarnings("unused")
public interface QuantumDocumentRepository extends MongoRepository<QuantumDocument,String> {

}
