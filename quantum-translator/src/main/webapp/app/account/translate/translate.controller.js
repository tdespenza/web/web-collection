/**
 * Created by tdespenza on 8/22/16.
 */

(function () {
    'use strict';

    angular
        .module('translatorApp')
        .controller('TranslateController', TranslateController);

    TranslateController.$inject = ['TranslateService', 'ExportService', 'QuantumDocument', '$scope', '$window', '_'];

    function TranslateController(TranslateService, ExportService, QuantumDocument, $scope, $window, _) {
        var vm = this;

        vm.error = null;
        vm.success = 'OK';
        vm.quantumDocument = {};

        vm.translateAll = translateAll;
        vm.translateSection = translateSection;
        vm.languages = languages;
        vm.direction = direction;
        vm.openExporter = openExporter;
        vm.save = save;

        $scope.to = {
            format: 'word',
            code: '',
            header: '',
            terms: '',
            facts: '',
            autograph: ''
        };
        $scope.from = {
            code: '',
            header: '',
            terms: '',
            facts: '',
            autograph: ''
        };
        $scope.imageUrl = '';
        $scope.languages = [];
        $scope.headerDisabled = true;
        $scope.termsDisabled = true;
        $scope.factsDisabled = true;
        $scope.autographDisabled = true;

        vm.languages();

        function translateAll() {
            var data = {
                toCode: $scope.to.code,
                fromCode: $scope.from.code,
                header: stripHtml($scope.from.header),
                terms: stripHtml($scope.from.terms),
                facts: stripHtml($scope.from.facts),
                autograph: stripHtml($scope.from.autograph)
            };

            TranslateService.translateAll(data).then(function (response) {
                $scope.to = response.data;
                $scope.to.format = 'word';
                $scope.to.code = response.data.toCode;
                $scope.from.code = response.data.fromCode;

                $scope.headerDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.header);
                $scope.termsDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.terms);
                $scope.factsDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.facts);
                $scope.autographDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.autograph);
            });
        }

        function translateSection(section, text) {
            var data = {
                toCode: $scope.to.code,
                fromCode: $scope.from.code,
                text: stripHtml(text)
            };

            TranslateService.translateSection(data).then(function (response) {
                if (section === 'header') {
                    $scope.to.header = response.data;
                    return;
                }

                if (section === 'terms') {
                    $scope.to.terms = response.data;
                    return;
                }

                if (section === 'facts') {
                    $scope.to.facts = response.data;
                    return;
                }

                if (section === 'autograph') {
                    $scope.to.autograph = response.data;
                    return;
                }
            });
        }

        function languages() {
            TranslateService.getAllLanguages().then(function (response) {
                $scope.languages = response.languages;
            });
        }

        function direction(direction, code) {
            if (direction === 'from') {
                $scope.from.code = code;
            }

            if (direction === 'to') {
                $scope.to.code = code;
                $scope.imageUrl = 'bower_components/flag-svg-collection/flags/4x3/' + code + '.svg';
            }
        }

        function openExporter() {
            vm.quantumDocument.code = $scope.to.code;
            vm.quantumDocument.imageUrl = $scope.imageUrl;
            vm.quantumDocument.format = $scope.to.format;
            vm.quantumDocument.text = {
                header: $scope.to.header,
                terms: $scope.to.terms,
                facts: $scope.to.facts,
                autograph: $scope.to.autograph
            };

            ExportService.createQuantumDocument(vm.quantumDocument).then(function (response) {
                if (response) {
                    ExportService.getQuantumDocument(response.filename, $scope.to.format).then(function (blob) {
                        var url = $window.URL || $window.webkitURL;

                        vm.quantumDocument.fileUrl = url.createObjectURL(blob);
                        vm.quantumDocument.filename = response.filename;

                        ExportService.open(vm.quantumDocument);
                    });
                }

            }).catch(function (error) {
                console.log(error);
            });

        }

        function save() {
            vm.isSaving = true;
            vm.quantumDocument.text = $scope.to.text;
            vm.quantumDocument.code = $scope.to.code;
            vm.quantumDocument.flag = $scope.flag;

            if (vm.quantumDocument.id !== null) {
                QuantumDocument.update(vm.quantumDocument, onSaveSuccess, onSaveError);

            } else {
                QuantumDocument.save(vm.quantumDocument, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('translatorApp:quantumDocumentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function stripHtml(text) {
            $.ready(function () {
                $('p,span,br').each(function () {
                    var attributes = this.attributes,
                        i = attributes.length;

                    while (i--) {
                        this.removeAttributeNode(attributes[i]);
                    }
                })
            });

            return text.replace(/((?!<((\/)?p|br|span|"))<[^>]*>)/gi, '');
        }

        $scope.fromHeaderSetup = function ($element) {
            $element.attr('name', 'fromHeader');
        };

        $scope.$watch('from.header', function () {
            $scope.headerDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.header);
        });

        $scope.$watch('from.terms', function () {
            $scope.termsDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.terms);
        });

        $scope.$watch('from.facts', function () {
            $scope.factsDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.facts);
        });

        $scope.$watch('from.autograph', function () {
            $scope.autographDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.autograph);
        });

        $scope.$watch('to.code', function () {
            $scope.headerDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.header);
            $scope.termsDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.terms);
            $scope.factsDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.facts);
            $scope.autographDisabled = _.isEmpty($scope.to.code) || _.isEmpty($scope.from.autograph);
        });

        $('textarea').attr('rows', 30);
    }
})();
