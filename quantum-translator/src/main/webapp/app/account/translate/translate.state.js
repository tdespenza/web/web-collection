/**
 * Created by tdespenza on 8/22/16.
 */

(function () {
    'use strict';

    angular
        .module('translatorApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('translate', {
            parent: 'account',
            url: '/translate',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'translate.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/account/translate/translate.html',
                    controller: 'TranslateController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('translate');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
