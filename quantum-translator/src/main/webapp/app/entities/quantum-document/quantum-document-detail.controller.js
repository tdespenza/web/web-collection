(function() {
    'use strict';

    angular
        .module('translatorApp')
        .controller('QuantumDocumentDetailController', QuantumDocumentDetailController);

    QuantumDocumentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'QuantumDocument'];

    function QuantumDocumentDetailController($scope, $rootScope, $stateParams, previousState, entity, QuantumDocument) {
        var vm = this;

        vm.quantumDocument = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('translatorApp:quantumDocumentUpdate', function(event, result) {
            vm.quantumDocument = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
