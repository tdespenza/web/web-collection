(function() {
    'use strict';

    angular
        .module('translatorApp')
        .controller('QuantumDocumentDeleteController',QuantumDocumentDeleteController);

    QuantumDocumentDeleteController.$inject = ['$uibModalInstance', 'entity', 'QuantumDocument'];

    function QuantumDocumentDeleteController($uibModalInstance, entity, QuantumDocument) {
        var vm = this;

        vm.quantumDocument = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            QuantumDocument.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
