(function () {
    'use strict';

    angular
        .module('translatorApp')
        .controller('QuantumDocumentDialogController', QuantumDocumentDialogController);

    QuantumDocumentDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'QuantumDocument'];

    function QuantumDocumentDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, QuantumDocument) {
        var vm = this;

        vm.quantumDocument = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.quantumDocument.id !== null) {
                QuantumDocument.update(vm.quantumDocument, onSaveSuccess, onSaveError);
            } else {
                QuantumDocument.save(vm.quantumDocument, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('translatorApp:quantumDocumentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }
    }
})();
