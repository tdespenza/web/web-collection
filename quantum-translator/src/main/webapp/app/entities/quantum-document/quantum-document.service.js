(function() {
    'use strict';
    angular
        .module('translatorApp')
        .factory('QuantumDocument', QuantumDocument);

    QuantumDocument.$inject = ['$resource'];

    function QuantumDocument ($resource) {
        var resourceUrl =  'api/quantum-documents/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
