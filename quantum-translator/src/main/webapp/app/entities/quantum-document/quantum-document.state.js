(function() {
    'use strict';

    angular
        .module('translatorApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('quantum-document', {
            parent: 'entity',
            url: '/quantum-document?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'translatorApp.quantumDocument.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/quantum-document/quantum-documents.html',
                    controller: 'QuantumDocumentController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('quantumDocument');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('quantum-document-detail', {
            parent: 'entity',
            url: '/quantum-document/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'translatorApp.quantumDocument.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/quantum-document/quantum-document-detail.html',
                    controller: 'QuantumDocumentDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('quantumDocument');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'QuantumDocument', function($stateParams, QuantumDocument) {
                    return QuantumDocument.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'quantum-document',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('quantum-document-detail.edit', {
            parent: 'quantum-document-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/quantum-document/quantum-document-dialog.html',
                    controller: 'QuantumDocumentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['QuantumDocument', function(QuantumDocument) {
                            return QuantumDocument.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('quantum-document.new', {
            parent: 'quantum-document',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/quantum-document/quantum-document-dialog.html',
                    controller: 'QuantumDocumentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                user: null,
                                text: null,
                                title: null,
                                autographers: null,
                                flag: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('quantum-document', null, { reload: 'quantum-document' });
                }, function() {
                    $state.go('quantum-document');
                });
            }]
        })
        .state('quantum-document.edit', {
            parent: 'quantum-document',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/quantum-document/quantum-document-dialog.html',
                    controller: 'QuantumDocumentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['QuantumDocument', function(QuantumDocument) {
                            return QuantumDocument.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('quantum-document', null, { reload: 'quantum-document' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('quantum-document.delete', {
            parent: 'quantum-document',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/quantum-document/quantum-document-delete-dialog.html',
                    controller: 'QuantumDocumentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['QuantumDocument', function(QuantumDocument) {
                            return QuantumDocument.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('quantum-document', null, { reload: 'quantum-document' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
