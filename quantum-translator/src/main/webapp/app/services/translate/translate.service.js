/**
 * Created by tdespenza on 8/23/16.
 */

(function () {
    'use strict';

    angular
        .module('translatorApp')
        .factory('TranslateService', TranslateService);

    TranslateService.$inject = ['$http'];

    function TranslateService($http) {
        var service = {
            translateAll: translateAll,
            translateSection: translateSection,
            getAllLanguages: getAllLanguages
        };

        var headers = {
            accept: 'application/json',
            'content-type': 'application/json'
        };

        function translateAll(data) {
            return $http({
                url: 'api/translate/all',
                method: 'POST',
                data: data,
                headers: headers
            }).then(function (result) {
                console.log(result);
                if (result.data) {
                    return {
                        data: result.data
                    };
                }
            });
        }

        function translateSection(data) {
            return $http({
                url: 'api/translate/section',
                method: 'POST',
                data: data,
                headers: headers
            }).then(function (result) {
                console.log(result);
                if (result.data) {
                    return {
                        data: result.data
                    };
                }
            });
        }

        function getAllLanguages() {
            return $http.get('api/languages').then(function (result) {
                if (result.data) {
                    return {
                        languages: result.data
                    };
                }
            });
        }

        return service;
    }
})();
