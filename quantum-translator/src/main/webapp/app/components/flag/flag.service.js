/**
 * Created by tdespenza on 9/24/16.
 */

(function () {
    'use strict';

    angular
        .module('translatorApp')
        .factory('FlagService', FlagService);

    FlagService.$inject = ['$http'];

    function FlagService($http) {
        var service = {
            createFlagImageUrls: createFlagImageUrls
        };

        function createFlagImageUrls() {
            return $http.get('bower_components/Countries/countries.minimal.json').then(function (result) {
                if (result) {
                    var flags = [];

                    $.each(result.data, function(key, value) {
                        flags.push({
                            "countryCode": key.toLowerCase(),
                            "country": value,
                            "imageUrl": "bower_components/flag-svg-collection/flags/4x3/" + key.toLowerCase() + ".svg"
                        });
                    });

                    return {
                        flags: flags
                    };
                }
            });
        }

        return service;
    }
})();
