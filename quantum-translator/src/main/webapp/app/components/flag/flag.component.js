/**
 * Created by tdespenza on 8/30/16.
 */

(function () {
    'use strict';

    var flag = {
        templateUrl: 'app/components/flag/flag.html',
        bindings: {
            imageUrl: '='
        },
        controller: FlagController
    };

    angular
        .module('translatorApp')
        .component('flag', flag);

    FlagController.$inject = ['JhiTrackerService', 'FlagService'];

    function FlagController(JhiTrackerService, FlagService) {
        var vm = this;

        vm.flags = [];

        vm.updateSelected = updateSelected;

        FlagService.createFlagImageUrls().then(function (result) {
            vm.flags = result.flags;
        });

        function updateSelected(imageUrl) {
            vm.imageUrl = imageUrl;
        }
    }
})();
