/**
 * Created by tdespenza on 9/28/16.
 */
(function () {
    'use strict';

    angular
        .module('translatorApp')
        .factory('ExportService', ExportService);

    ExportService.$inject = ['$http', '$uibModal'];

    function ExportService($http, $uibModal) {
        var service = {
            open: open,
            createQuantumDocument: createQuantumDocument,
            getQuantumDocument: getQuantumDocument
        };

        var modalInstance = null;
        var resetModal = function () {
            modalInstance = null;
        };

        function open(quantumDocument) {
            if (modalInstance !== null) return;

            modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/components/export/export.html',
                controller: 'ExportController',
                controllerAs: 'vm',
                resolve: {
                    quantumDocument: function () {
                        return quantumDocument;
                    },
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('export');
                        return $translate.refresh();
                    }]
                }
            });

            modalInstance.result.then(
                resetModal,
                resetModal
            );
        }

        function createQuantumDocument(data) {
            if (data.format === null || data.format === '') return null;

            return $http({
                url: 'api/export/' + data.format + '/create',
                method: 'POST',
                data: data
            }).then(function (result) {
                if (result.data) {
                    return result.data;
                }
            });
        }

        function getQuantumDocument(filename, format) {
            if (filename === null || filename === '') return null;
            if (format === null || format === '') return null;

            return $http.get('api/export/' + format + '/' + filename, {responseType: 'arraybuffer'}).then(function (result) {
                console.log(result.data);

                if (result.data) {
                    return new Blob([result.data], {type: getMimeType(format)});
                }
            });
        }

        function getMimeType(format) {
            if (format === 'word') {
                return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
            }

            if (format === 'pdf') {
                return 'application/pdf';
            }

            if (format === 'plain') {
                return 'text/plain';
            }
        }

        return service;
    }
})();
