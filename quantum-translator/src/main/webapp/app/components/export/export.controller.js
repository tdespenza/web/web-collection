/**
 * Created by tdespenza on 9/28/16.
 */
(function () {
    'use strict';

    angular
        .module('translatorApp')
        .controller('ExportController', ExportController);

    ExportController.$inject = ['$rootScope', '$uibModalInstance', 'quantumDocument'];

    function ExportController($rootScope, $uibModalInstance, quantumDocument) {
        var vm = this;

        $rootScope.format = "word";
        $rootScope.text = quantumDocument.text;
        $rootScope.imageUrl = quantumDocument.imageUrl;
        $rootScope.fileUrl = quantumDocument.fileUrl;
        $rootScope.filename = quantumDocument.filename;

        vm.close = close;

        function close() {
            $uibModalInstance.close();
        }
    }
})();
