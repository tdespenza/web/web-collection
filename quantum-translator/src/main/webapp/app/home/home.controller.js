(function () {
    'use strict';

    angular
        .module('translatorApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$rootScope', '$scope', 'Principal', 'LoginService', 'Auth', '$state', '$location'];

    function HomeController($rootScope, $scope, Principal, LoginService, Auth, $state, $location) {
        var vm = this;

        vm.authenticationError = false;
        vm.credentials = {};
        vm.password = null;
        vm.rememberMe = true;
        vm.username = null;
        vm.account = null;
        vm.isAuthenticated = null;

        vm.cancel = cancel;
        vm.login = login;
        vm.requestResetPassword = requestResetPassword;

        $scope.$on('authenticationSuccess', function () {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }

        function login(event) {
            event.preventDefault();
            Auth.login({
                username: vm.username,
                password: vm.password,
                rememberMe: vm.rememberMe
            }).then(function () {
                vm.authenticationError = false;

                if ($state.current.name === 'activate' ||
                    $state.current.name === 'finishReset' || $state.current.name === 'requestReset') {
                    $state.go('home');
                }

                $rootScope.$broadcast('authenticationSuccess');

                // previousState was set in the authExpiredInterceptor before being redirected to login modal.
                // since login is successful, go to stored previousState and clear previousState
                if (Auth.getPreviousState()) {
                    var previousState = Auth.getPreviousState();
                    Auth.resetPreviousState();
                    $state.go(previousState.name, previousState.params);
                }

                $state.go('translate');

            }).catch(function () {
                vm.authenticationError = true;
            });
        }

        function cancel () {
            vm.credentials = {
                username: null,
                password: null,
                rememberMe: true
            };

            vm.authenticationError = false;
        }

        function requestResetPassword () {
            $state.go('requestReset');
        }
    }
})();
